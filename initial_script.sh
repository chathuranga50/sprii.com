#!/bin/bash

AWS_ACCESS_KEY_ID='XXXXXXXXXXXXXXXXXX'
AWS_SECRET_ACCESS_KEY='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
REGION='xx-xxxx-1'
S3_BUCKET='xxxxxxxxxxx'

find ./ -name "*" -type f -exec sed -i "s/AKIAJSVVVWSRENDH6ACQ/$AWS_ACCESS_KEY_ID/g" {} \;
find ./ -name "*" -type f -exec sed -i "s/Admqtnf4QZTshn23BcEyHZegMIqI7uIAZU51FLm+/$AWS_SECRET_ACCESS_KEY/g" {} \;
find ./ -name "*" -type f -exec sed -i "s/us-east-1/$REGION/g" {} \;
find ./ -name "*" -type f -exec sed -i "s/php-app-packages/$S3_BUCKET/g" {} \;

cd terraform
terraform apply

cd ../
ansible-playbook -i /etc/ansible/ec2.py sprii-jenkins-config.yml --private-key=./AWS_Test.pem --limit tag_Name_Jenkins_APP -v

echo -e "*==================================================================*\n"

echo -e " Jenkins has been installed and configued successfully!"
echo -e " Navigate to http://PUB_IP:8080 in your browser to access the application.\n"

echo -e " Login with the default credentials:"
echo -e " Username - admin"
echo -e " Password - admin"

echo -e "*==================================================================*\n"

echo -e "sprii-app-build - for new package build"
echo -e "sprii-app-deploy - for application deploy to servers"

echo -e " Script written by Chathuranga Abeyrathna, 2018."

echo -e "*==================================================================*"