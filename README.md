# AWS Solution for PHP Application

I this set of scripts we are planning to spin up all the AWS infrastructure level chnages using Terraform, Configuration management using Ansible and CI tool as Jenkins.

# Prerequirements
In running PC/Server of this script needs to met following requirements.
```
- Terraform (https://www.terraform.io/intro/getting-started/install.html)
- Ansible (http://docs.ansible.com/ansible/latest/intro_installation.html)
```

Once Ansible and Terraform installed you need to configure to support AWS Dynamic inventory scrips.
```
- Download below two files
https://github.com/ansible/ansible/blob/devel/contrib/inventory/ec2.py
https://github.com/ansible/ansible/blob/stable-1.9/plugins/inventory/ec2.ini
```
- Place those in /etc/ansible and add executable permissions (chmod +x /etc/ansible/ec2.py)


# Run Script
Update below script values,
```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
REGION
S3_BUCKET
```

to run,
`./initial_script.sh`


# Information

```
Application codebase - https://github.com/chaturanga50/php-sample
Automation codebase (Application + Infrastructure) - https://bitbucket.org/chathuranga50/sprii.com
```
