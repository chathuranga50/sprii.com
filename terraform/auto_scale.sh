#!/bin/bash

s3_bucket="php-app-packages"
s3_static_bucket="php-app-packages-download" #do not change as this feach initial package from this s3 which is publically accessible
region="us-east-1"

rm -rf /tmp/latest_package.txt
aws s3 --region $region cp s3://$s3_bucket/latest_package.txt /tmp/
package=$(cat /tmp/latest_package.txt)
yum update -y
yum install httpd php -y
cd /tmp/

if [ ! -f /tmp/latest_package.txt ];
then
    echo "$package file not found"
    wget https://s3.amazonaws.com/php-app-packages-download/sprii-app-sprii-frontend-1.0-base.x86_64.rpm
    yum install /tmp/sprii-app-sprii-frontend-1.0-base.x86_64.rpm -y
else
    echo "$package file found"
    yum remove sprii-app-sprii-frontend-* -y
    yum install $package -y
fi

chown apache.apache /var/www/html -R
/etc/init.d/httpd restart