provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "Allow_Traffic_PHP_APPs" {
  name        = "Allow_Traffic_PHP_APPs"
  description = "Allow inbound port 22 traffic to PHP App Server"

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

  tags {
    Name = "Allow_Traffic_PHP_APPs"
  }
}

resource "aws_security_group" "Allow_Traffic_Jenkins_APP" {
  name        = "Allow_Traffic_Jenkins_APP"
  description = "Allow inbound port 22 traffic to Jenkins server"

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

  tags {
    Name = "Allow_Traffic_Jenkins_APP"
  }
}

resource "aws_key_pair" "AWS_Server_Keypair" {
  key_name   = "AWS_Server_Keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCp2cfOF/I5ETGmYL/teeGS53GgaN0tg/WS16GHZio3DJ5V1gykwX085YXu4EkHjKd6ipBu6kLb34rmy57Bif5zNZxK4wSLsyRi8jK1CnlUEg/vRZ2NgTAoapwEAbNqKVZymwKieBfY4YLlcubm1gYYzs9HFUtdtxp30ZLmetil6Em/hLYu24d3yXRF6RNQYpQI4WJmXKAu88rXGypgN2s5xpoRvyZxM8JS+Lkkzxv+0L8Ltf3HvaOjhkjKGK3If37mCUXmQbI5fefwmrKakk9sZB+SAYKZyiZG+/5HbhzbT9/74a2fCjKpKFmCm9+Rm/7H3A5H4ieDhiWodJVdQzOl OpenSSH-rsa-import-030918"
}

resource "aws_s3_bucket" "bucket-php-aws" {
  bucket = "${var.bucket_name}"
  acl    = "private"

  tags {
    Name        = "Build files"
    Environment = "PROD"
  }
}

resource "aws_iam_role" "web_iam_role" {
	    name = "web_iam_role"
	    assume_role_policy = <<EOF
{
	  "Version": "2012-10-17",
	  "Statement": [
      {
      "Action": "sts:AssumeRole",
      "Principal": {
      "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
      }
	  ]
}
EOF
}

resource "aws_iam_instance_profile" "php-web_instance_profile" {
  	    name = "php-web_instance_profile"
        role = "web_iam_role"
}

resource "aws_iam_role_policy" "web_iam_role_policy" {
  	  name = "web_iam_role_policy"
  	  role = "${aws_iam_role.web_iam_role.id}"
  	  policy = <<EOF
{
  	  "Version": "2012-10-17",
      "Statement": [
        {
            "Effect": "Allow",
            "Action": ["s3:ListBucket"],
    	      "Resource": ["arn:aws:s3:::${var.bucket_name}"]
        },
        {
            "Effect": "Allow",
            "Action": [
              "s3:PutObject",
              "s3:GetObject",
              "s3:DeleteObject"
              	      ],
    	      "Resource": ["arn:aws:s3:::${var.bucket_name}/*"]
        }
        	  ]
}
EOF
}

resource "aws_instance" "web" {
  ami           = "ami-97785bed"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.Allow_Traffic_PHP_APPs.id}"]
  key_name = "AWS_Server_Keypair"
  user_data = "${file("auto_scale.sh")}"

  tags {
    Name = "PHP_Static_APP"
  }
  iam_instance_profile = "${aws_iam_instance_profile.php-web_instance_profile.id}"
}

resource "aws_instance" "jenkins" {
  ami           = "ami-97785bed"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.Allow_Traffic_Jenkins_APP.id}"]
  key_name = "AWS_Server_Keypair"

  tags {
    Name = "Jenkins_APP"
  }
  iam_instance_profile = "${aws_iam_instance_profile.php-web_instance_profile.id}"
}

resource "aws_launch_configuration" "php-web-lc" {
    name_prefix = "php-web-lc-"
    image_id = "ami-97785bed"
    instance_type = "t2.micro"
    security_groups = ["${aws_security_group.Allow_Traffic_PHP_APPs.id}"]
    key_name = "AWS_Server_Keypair"
    user_data = "${file("auto_scale.sh")}"
    iam_instance_profile = "${aws_iam_instance_profile.php-web_instance_profile.id}"

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "php-web" {
    availability_zones = ["us-east-1a"]
    name = "php-web"
    max_size = "5"
    min_size = "1"
    health_check_grace_period = 300
    health_check_type = "EC2"
    desired_capacity = 1
    force_delete = true
    launch_configuration = "${aws_launch_configuration.php-web-lc.name}"

    tag {
        key = "Name"
        value = "PHP_Scale_APP"
        propagate_at_launch = true
    }
}

resource "aws_autoscaling_policy" "php-web-scale-up" {
    name = "php-web-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.php-web.name}"
}

resource "aws_autoscaling_policy" "php-web-scale-down" {
    name = "php-web-scale-down"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.php-web.name}"
}

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
    alarm_name = "cpu-util-high-php-web"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "System/EC2"
    period = "300"
    statistic = "Average"
    threshold = "80"
    alarm_description = "This metric monitors ec2 cpu for high utilization on php-web hosts"
    alarm_actions = [
        "${aws_autoscaling_policy.php-web-scale-up.arn}"
    ]
    dimensions {
        AutoScalingGroupName = "${aws_autoscaling_group.php-web.name}"
    }
}

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
    alarm_name = "cpu-util-low-php-web"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "System/EC2"
    period = "300"
    statistic = "Average"
    threshold = "40"
    alarm_description = "This metric monitors ec2 cpu for low utilization on php-web hosts"
    alarm_actions = [
        "${aws_autoscaling_policy.php-web-scale-down.arn}"
    ]
    dimensions {
        AutoScalingGroupName = "${aws_autoscaling_group.php-web.name}"
    }
}

resource "aws_elb" "web-elb" {
  name               = "php-web-elb"
  availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
  security_groups = ["${aws_security_group.Allow_Traffic_PHP_APPs.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = ["${aws_instance.web.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name = "php-web-elb"
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_web_scale" {
  autoscaling_group_name = "${aws_autoscaling_group.php-web.id}"
  elb                    = "${aws_elb.web-elb.id}"
}