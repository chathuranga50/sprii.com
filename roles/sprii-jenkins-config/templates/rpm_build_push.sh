#!/bin/bash

# See below for usage.

set -e
set -u

# Error handling.
die () {
	echo "Error:  $*" > /dev/stderr
	exit 1
}

trap '{ die "An error occurred in rpm_build_rpm.sh" }' err


# Usage.
usage () {
	cat <<-eof
Usage:
rpm_build_rpm.sh options

Where options are:
--specfile specfile             - Path to rpm spec file.  Required.
--topdir topdir                 - Path to rpm top directory.  Required.
--sourcedir sourcedir           - Path to source directory.  Required.
--suffix suffix                 - Suffix to append to rpm name.  Optional.
--version version               - 'x.y.z' style rpm version.  Required.
--versionfile versionfile       - Versionfile to be add in to the rpm.  Required.
--release release               - 'r' style rpm release number.  Required.
--repository repository         - Required.
	eof
}


# Parse command line.
[ $# -eq 0 ] && usage && exit 0
[ $# -eq 1 -a "${1:-}" = "--help" ] && usage && exit 0

# Defaults.
opt_suffix="%{nil}"

while [ $# -ge 2 ]; do
        eval opt_${1#--}=\"$2\"
        shift 2
done

for option in specfile topdir sourcedir version release repository; do
        v=opt_$option
	[ "${!v:-}" == "" ] && die "Missing option $option"
done

# Prepare.
for directory in BUILD RPMS SOURCES SPECS SRPMS; do
	mkdir -p $opt_topdir/$directory
done

echo $opt_suffix-$opt_version-$opt_release > $opt_versionfile

echo
echo
echo "-- htaccess fix"
HTACCESS_FILE=$(/bin/ls $opt_sourcedir/.htaccess | wc -l)
if [ "$HTACCESS_FILE" -eq "1" ];
    then
	echo ".htaccess file available"
    cp $opt_sourcedir/.htaccess $opt_sourcedir/htaccess
    else
    echo "htaccess not there :("
fi

echo
echo
echo "-- Build:"
rpmbuild -bb $opt_specfile \
	--define="_topdir $opt_topdir" \
	--define="opt_sourcedir `readlink -f $opt_sourcedir`" \
	--define="opt_suffix $opt_suffix" \
	--define="opt_version $opt_version" \
	--define="opt_release $opt_release"

echo
echo
echo "-- Publish:"

for file in $opt_topdir/RPMS/*/*; do
	echo $file
    aws s3 cp $file s3://$opt_repository/
done

echo
echo
echo "-- Cleanup:"
rm -rfv $opt_topdir

exit 0
