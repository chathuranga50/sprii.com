%define applicationdir /var/www/html/

%if "%{opt_suffix}" == ""
Name: sprii-app
%else
Name: sprii-app-%{opt_suffix}
%endif

Version: %{opt_version}
Release: %{opt_release}
Group: Sprii
URL: http://sprii.com
Summary: Sprii - PHP Test app.
Vendor: Sprii
License: Sprii
Packager: Chathuranga Abeyrathna <chaturanga50@gmail.com>

BuildRoot: /var/tmp/%{name}-buildroot
AutoReqProv: no
Requires: php
BuildArch: x86_64


%description
Sprii - PHP Test app.


%prep
:

%build
: 

%install
rm -rf $RPM_BUILD_ROOT/%{applicationdir}
mkdir -p $RPM_BUILD_ROOT/%{applicationdir}

cp -a %{opt_sourcedir}/* $RPM_BUILD_ROOT/%{applicationdir}/.

echo RPM_BUILD_ROOT is $RPM_BUILD_ROOT


%post

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-, root, root)
%{applicationdir}/*


%pre
:

%changelog
* Thu Mar 09 2018 Chathuranga Abeyrathna <chaturanga50@gmail.com>
- Created spec along with the build job.