Role Name
=========

Create EC2 instance in AWS

Requirements
------------

You need to setup AWS Key and Key Secret in localhost to connect to AWS.

`cat ~/.aws/credentials
AWS_ACCESS_KEY_ID="XXXXXXXXXXXXXXXXXXXX"
AWS_SECRET_ACCESS_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
AWS_DEFAULT_REGION="us-west-1"`

Role Variables
--------------

`key_name: "key_name"
instance_type: "t2.micro"
image_id: "ami id"
region_type: "region"
server_count: "server count"
subnet_id: "subnet id"
security_group: "security group"
name_tag: "name tag"`

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      connection: local
      gather_facts: False
      
      roles:
         - { role: sprii-ec2-init, key_name: Production, instance_type: t2.micro, image_id: ami-d2c924b2, region_type: eu-west-1, server_count: 1, subnet_id: subnet-bb25c6df, security_group: prod, name_tag: APPs }

License
-------

BSD

Author Information
------------------

Chathuranga Abeyrathna (chaturanga50@gmail.com)